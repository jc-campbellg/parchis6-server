const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const axios = require("axios");

const port = process.env.PORT || 4000;
const index = require("./routes/index");

const app = express();
app.use(index);

const server = http.createServer(app);
const io = socketIo(server); // < Interesting!

connections = [];

io.on("connection", (socket)=>{
  connections.push(socket);
  socket.room='$LOBBY';
  socket.color='white';
  console.log("New client connected", socket.id);
  console.log("Clients online:", connections.length);
  socket.emit('id',socket.id);
  socket.on("disconnect", (data)=>{
    var index = connections.indexOf(socket);
    connections.splice(index, 1);
    console.log("Client disconnected", socket.nickname);
    console.log("Clients online:", connections.length);
  });
  socket.on('chat',(data)=>{
    io.in(socket.room).emit('chat',{
      name:data.name,
      message:data.message,
      type:data.type
    });
  });
  socket.on('getOnline',(data)=>{
    //Check if name exists in current room
    if (checkNameInRoom(data.name,data.room)){
      //Error choose a new Name
      socket.emit('chat',{
        name:'server',
        message:'El nombre ya existe',
        type:'error'
      });
    } else {
      //Suceed
      socket.nickname = data.name;
      socket.room = data.room;
      io.in(data.room).emit('chat',{
        name:'Server',
        message:data.name+" se ha unido a la mesa.",
        type:'suceed'
      });
      socket.join(data.room);
      socket.color = 'white';
      console.log(socket.nickname,"joins",data.room);
      socket.emit('ready',data.name, (newData)=>{
        console.log(socket.nickname,"is ready");
        socket.emit('chat',{
          name:'Server',
          message:'Bienvenido a la mesa '+data.room,
          type:'suceed'
        });
        var colors = checkAvalaibleColors(socket.room);
        socket.emit('colors',colors);
        if (playersInRoom(data.room) === 1) {
          socket.emit('role','admin');
        } else if (colors.length === 0) {
          socket.emit('role','spectator');
        } else {
          socket.emit('role','player');
        }
      });
    }
  });
  socket.on('selectColor',(color)=>{
    var colors = checkAvalaibleColors(socket.room);
    var found = false;
    for (var i = 0; i < colors.length; i++) {
      if (colors[i] === color) {
        //Color is free
        found = true;
        io.in(socket.room).emit('chat',{
          name:'Server',
          message:socket.nickname+' esta listo para jugar.',
          type:'suceed'
        });
        socket.color = color;
        console.log(socket.nickname,"pick",socket.color);
        socket.emit('playing',true);
        var newcolors = checkAvalaibleColors(socket.room);
        io.in(socket.room).emit('colors',newcolors);
      }
    }
    if (found === false){
      //You cant use that color
      socket.emit('chat',{
        name:'Server',
        message:'El color ya esta tomado',
        type:'error'
      });
      var newcolors = checkAvalaibleColors(socket.room);
      io.in(socket.room).emit('colors',newcolors);
    }
  });
});

var playersInRoom=(room)=>{
  var sum = 0;
  for (var i = 0; i < connections.length; i++) {
    if (connections[i].room === room){
      sum ++;
    }
  }
  return sum;
}

var checkAvalaibleColors=(room)=>{
  var colors = ['red','blue','yellow','green','purple','orange'];
  for (var i = 0; i < connections.length; i++) {
    if (connections[i].room === room){
      //Remove colors that are used
      var index = colors.indexOf(connections[i].color);
      if (index != -1){
          colors.splice(index, 1);
      }
    }
  }
  console.log(colors);
  return colors;
}

var checkNameInRoom=(name,room)=>{
  for (var i = 0; i < connections.length; i++) {
    if (connections[i].room === room){
      if (connections[i].nickname.toLowerCase() === name.toLowerCase()){
        return true;
      }
    }
  }
  return false;
}
/*
// sending to sender-client only
socket.emit('message', "this is a test");

// sending to all clients, include sender
io.emit('message', "this is a test");

// sending to all clients except sender
socket.broadcast.emit('message', "this is a test");

// sending to all clients in 'game' room(channel) except sender
socket.broadcast.to('game').emit('message', 'nice game');

// sending to all clients in 'game' room(channel), include sender
io.in('game').emit('message', 'cool game');

// sending to sender client, only if they are in 'game' room(channel)
socket.to('game').emit('message', 'enjoy the game');

// sending to all clients in namespace 'myNamespace', include sender
io.of('myNamespace').emit('message', 'gg');

// sending to individual socketid
socket.broadcast.to(socketid).emit('message', 'for your eyes only');
*/

server.listen(port, () => {
  console.log(`Listening on port ${port}`)
});
